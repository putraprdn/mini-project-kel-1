const express = require("express"); //inisiasi variable yang berisi express
const router = express.Router(); // inisiasi variable yang berisi fungsi router express
const {
	list,
	create,
	update,
	destroy,
	findById,
} = require("../controllers/categoryController"); // inisiasi object controller
const validate = require("../middleware/validate");
const { createCategoryRules } = require("../validators/rule");
const checkToken = require("../middleware/checkToken");
const {
	permissionRead,
	permissionWrite,
	permissionUpdate,
	permissionDelete,
} = require("../controllers/authController");

router.get("/list", checkToken, permissionRead, list); // route untuk endpoint list
router.post("/find-by-id", checkToken, permissionRead, findById); // route untuk endpoint list
router.post(
	"/create",
	checkToken,
	permissionWrite,
	validate(createCategoryRules),
	create
); // route untuk endpoint create
router.put("/update", checkToken, permissionUpdate, update); // route untuk endpoint update
router.delete("/destroy", checkToken, permissionDelete, destroy); // route untuk endpoint destroy

module.exports = router; // export fungsi router agar module lain bisa membaca file ini
