const { permission } = require("../models");

module.exports = {
	permissionRead(id) {
		return permission.findOne({
			where: {
				name: "read",
				roleId: id,
				model: "category",
			},
		});
	},
	permissionWrite(id) {
		return permission.findOne({
			where: {
				name: "write",
				roleId: id,
				model: "category",
			},
		});
	},
	permissionUpdate(id) {
		return permission.findOne({
			where: {
				name: "update",
				roleId: id,
				model: "category",
			},
		});
	},
	permissionDelete(id) {
		return permission.findOne({
			where: {
				name: "delete",
				roleId: id,
				model: "category",
			},
		});
	},
};
