const authRepository = require("../repositories/authRepository");

module.exports = {
	async permissionRead(userInfo) {
		try {
			const roleId = userInfo.roleId,
				isAllowed = await authRepository.permissionRead(roleId);

			if (!isAllowed) throw new Error("Restricted Access");

			return userInfo;
		} catch (err) {
			throw err;
		}
	},
	async permissionWrite(userInfo) {
		try {
			const roleId = userInfo.roleId,
				isAllowed = await authRepository.permissionWrite(roleId);

			if (!isAllowed) throw new Error("Restricted Access");

			return userInfo;
		} catch (err) {
			throw err;
		}
	},
	async permissionUpdate(userInfo) {
		try {
			const roleId = userInfo.roleId,
				isAllowed = await authRepository.permissionUpdate(roleId);

			if (!isAllowed) throw new Error("Restricted Access");

			return userInfo;
		} catch (err) {
			throw err;
		}
	},
	async permissionDelete(userInfo) {
		try {
			const roleId = userInfo.roleId,
				isAllowed = await authRepository.permissionDelete(roleId);

			if (!isAllowed) throw new Error("Restricted Access");

			return userInfo;
		} catch (err) {
			throw err;
		}
	},
};
