const categoryRepository = require("../repositories/categoryRepository");

module.exports = {
	getAll: () => categoryRepository.getAll(),
	findById: (id) => categoryRepository.findById(id),
	create: (data) => categoryRepository.create(data),
	update: (data) => categoryRepository.update(data),
	destroy: async (id) => {
		try {
			const dataExist = await categoryRepository.findById(id);

			if (!dataExist) throw Error("Data Doesn't Exist");

			await categoryRepository.destroy(id);
		} catch (error) {
			throw error;
		}
	},
};
