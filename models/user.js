"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class user extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			this.belongsTo(models.userRole, {
				foreignKey: "roleId",
				as: "userRole",
			});
		}
	}
	user.init(
		{
			roleId: {
				type: DataTypes.INTEGER,
			},
			firstName: DataTypes.STRING,
			lastName: DataTypes.STRING,
			email: DataTypes.STRING,
			username: DataTypes.STRING,
			password: DataTypes.STRING,
			isActive: {
				type: DataTypes.BOOLEAN,
				defaultValue: true,
			},
		},
		{
			sequelize,
			modelName: "user",
		}
	);
	return user;
};
