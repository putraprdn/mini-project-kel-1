"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class userRole extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			this.hasMany(models.user, {
				foreignKey: "roleId",
				as: "user",
			}),
				this.hasMany(models.permission, {
					foreignKey: "roleId",
					as: "permission",
				});
		}
	}
	userRole.init(
		{
			name: {
				type: DataTypes.STRING,
				allowNull: false,
			},
		},
		{
			sequelize,
			modelName: "userRole",
			timestamps: true,
		}
	);
	return userRole;
};
