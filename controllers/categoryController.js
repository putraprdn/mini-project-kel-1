const model = require("../models"), // inisiasi variable model yang berisi model dari folder models
	categoryService = require("../services/categoryService"),
	authController = require("./authController");

module.exports = {
	list: async (req, res) => {
		try {
			const datas = await categoryService.getAll();

			return res.status(200).json({
				success: true,
				error: 0,
				message: "data successfully listed",
				data: datas,
			});
		} catch (error) {
			return res.status(500).json({
				success: false,
				error: error.code,
				message: error,
				data: null,
			});
		}
	},
	findById: async (req, res) => {
		try {
			const datas = await categoryService.findById(req.body.id);

			return res.status(200).json({
				success: true,
				error: 0,
				message: "data successfully listed",
				data: datas,
			});
		} catch (error) {
			return res.status(500).json({
				success: false,
				error: error.code,
				message: error,
				data: null,
			});
		}
	},
	create: async (req, res) => {
		try {
			const data = await categoryService.create(req.body);

			return res.status(200).json({
				success: true,
				error: 0,
				message: "data successfully created",
				data: data,
			});
		} catch (error) {
			return res.status(500).json({
				success: false,
				error: error.code,
				message: error,
				data: null,
			});
		}
	},
	update: async (req, res) => {
		try {
			const data = await categoryService.update(req.body);

			return res.status(200).json({
				success: true,
				error: 0,
				message: "data successfully updated",
				data: data,
			});
		} catch (error) {
			return res.status(500).json({
				success: false,
				error: error.code,
				message: error,
				data: null,
			});
		}
	},
	destroy: async (req, res) => {
		try {
			const data = await categoryService.destroy(req.body.id);

			return res.status(200).json({
				success: true,
				error: 0,
				message: "data successfully deleted",
				data: data,
			});
		} catch (error) {
			return res.status(500).json({
				success: false,
				error: error.code,
				message: error.message,
				data: null,
			});
		}
	},
};
