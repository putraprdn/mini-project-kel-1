const authService = require("../services/authService");

module.exports = {
	// Middleware
	async permissionRead(req, res, next) {
		try {
			const accessGranted = await authService.permissionRead(
				res.locals.user
			);

			if (!accessGranted) throw new Error("Unauthorized Access");
			res.locals.user = accessGranted;
			next();
		} catch (err) {
			return res.status(403).json({
				success: false,
				error: 403,
				message: err.message,
				data: null,
			});
		}
	},
	async permissionWrite(req, res, next) {
		try {
			const accessGranted = await authService.permissionWrite(
				res.locals.user
			);

			if (!accessGranted) throw new Error("Unauthorized Access");
			res.locals.user = accessGranted;
			next();
		} catch (err) {
			return res.status(403).json({
				success: false,
				error: 403,
				message: err.message,
				data: null,
			});
		}
	},
	async permissionUpdate(req, res, next) {
		try {
			const accessGranted = await authService.permissionUpdate(
				res.locals.user
			);

			if (!accessGranted) throw new Error("Unauthorized Access");
			res.locals.user = accessGranted;
			next();
		} catch (err) {
			return res.status(403).json({
				success: false,
				error: 403,
				message: err.message,
				data: null,
			});
		}
	},
	async permissionDelete(req, res, next) {
		try {
			const accessGranted = await authService.permissionDelete(
				res.locals.user
			);

			if (!accessGranted) throw new Error("Unauthorized Access");
			res.locals.user = accessGranted;
			next();
		} catch (err) {
			return res.status(403).json({
				success: false,
				error: 403,
				message: err.message,
				data: null,
			});
		}
	},
};
