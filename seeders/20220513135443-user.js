"use strict";

module.exports = {
	async up(queryInterface, Sequelize) {
		return queryInterface.bulkInsert("users", [
			{
				id: 1,
				roleId: 1,
				firstName: "Admin 1",
				lastName: "New",
				email: "admin1@crm.com",
				username: "admin1",
				password:
					"$2b$12$kZVEHxo7DFAsZPmg1xaNnOsKcF2aInJoHDV9AtRVIgI913dScfppq", //123456
				isActive: true,
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				id: 2,
				roleId: 2,
				firstName: "Writer 1",
				lastName: "New",
				email: "writer1@crm.com",
				username: "writer1",
				password:
					"$2b$12$kZVEHxo7DFAsZPmg1xaNnOsKcF2aInJoHDV9AtRVIgI913dScfppq", //123456
				isActive: true,
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				id: 3,
				roleId: 3,
				firstName: "Reader 1",
				lastName: "New",
				email: "reader1@crm.com",
				username: "reader1",
				password:
					"$2b$12$kZVEHxo7DFAsZPmg1xaNnOsKcF2aInJoHDV9AtRVIgI913dScfppq", //123456
				isActive: true,
				createdAt: new Date(),
				updatedAt: new Date(),
			},
		]);
	},

	//  roleId: {
	//   type: DataTypes.INTEGER,
	//   defaultValue: 3,
	// },
	// firstName: DataTypes.STRING,
	// lastName: DataTypes.STRING,
	// email: DataTypes.STRING,
	// username: DataTypes.STRING,
	// password: DataTypes.STRING,
	// isActive: {
	//   type: DataTypes.BOOLEAN,
	//   defaultValue: true
	// }

	async down(queryInterface, Sequelize) {
		/**
		 * Add commands to revert seed here.
		 *
		 * Example:
		 * await queryInterface.bulkDelete('People', null, {});
		 */
	},
};
