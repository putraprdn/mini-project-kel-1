"use strict";

module.exports = {
	async up(queryInterface, Sequelize) {
		return queryInterface.bulkInsert("permissions", [
			{
				id: 1,
				roleId: 1,
				name: "write",
				model: "category",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				id: 2,
				roleId: 1,
				name: "read",
				model: "category",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				id: 3,
				roleId: 1,
				name: "update",
				model: "category",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				id: 4,
				roleId: 1,
				name: "delete",
				model: "category",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				id: 5,
				roleId: 2,
				name: "write",
				model: "category",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				id: 6,
				roleId: 2,
				name: "read",
				model: "category",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				id: 7,
				roleId: 2,
				name: "update",
				model: "category",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				id: 8,
				roleId: 3,
				name: "read",
				model: "category",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
		]);
	},

	async down(queryInterface, Sequelize) {
		/**
		 * Add commands to revert seed here.
		 *
		 * Example:
		 * await queryInterface.bulkDelete('People', null, {});
		 */
	},
};
