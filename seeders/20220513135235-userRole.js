'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
		return queryInterface.bulkInsert("userRoles", [
			{
				id: 1,
				name: "Admin",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				id: 2,
				name: "Writer",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				id: 3,
				name: "Reader",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
		]);
	},

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
